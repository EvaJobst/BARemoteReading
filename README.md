# Remote Reading

„Vorlesen verbindet“ (reading connects) enables far-away relatives to stay in touch with their grandchildren or nephews/nieces and read books to them remotely. The core parts are a synchronized reading mode with an active video connection.

It started out as a project and the app has been expanded with gestures and interaction techniques for the bachelor thesis.

__Libraries:__ Pubnub, WebRTC, Butterknife, Picasso, GSON
<br/><br/>


03/2017 – 03/2018 | Android Tablet
