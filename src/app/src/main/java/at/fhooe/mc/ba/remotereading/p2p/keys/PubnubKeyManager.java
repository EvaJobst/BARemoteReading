package at.fhooe.mc.ba.remotereading.p2p.keys;

public class PubnubKeyManager implements PubnubKeyProvider {
    private String pubKey;
    private String subkey;

    public PubnubKeyManager() {
        setPubKey();
        setSubKey();
    }

    @Override
    public String getPubKey() {
        return pubKey;
    }

    @Override
    public void setPubKey() {
        pubKey = "pub-c-473645fa-cb87-4322-a270-4b8da04c1782";
    }

    @Override
    public String getSubKey() {
        return subkey;
    }

    @Override
    public void setSubKey() {
        subkey = "sub-c-1bb5f3fc-70a8-11e7-958a-0619f8945a4f";
    }
}
