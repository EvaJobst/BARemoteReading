package at.fhooe.mc.ba.remotereading.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.data.Book;
import at.fhooe.mc.ba.remotereading.interfaces.BookDetailDialogFragmentListener;
import at.fhooe.mc.ba.remotereading.ui.activity.VideoConnectionActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookDetailDialogFragment extends DialogFragment implements Observer {

    public static final String KEY_ARGUMENT_BOOK = "keyArgumentBook";

    @BindView(R.id.image_book_detail_thumbnail)
    protected ImageView imageViewThumbnail;

    @BindView(R.id.txt_book_detail_title)
    protected TextView textViewTitle;

    @BindView(R.id.txt_book_detail_publisher)
    protected TextView textViewPublisher;

    @BindView(R.id.txt_book_detail_short_description)
    protected TextView textViewShortDescription;

    private BookDetailDialogFragmentListener listener = null;
    private Book selectedBook;
    private Activity activity;

    public BookDetailDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        listener = ((BookDetailDialogFragmentListener) getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_book_detail, null);
        ButterKnife.bind(this, view);

        activity = getActivity();

        if(((VideoConnectionActivity) activity).isOnlineModeActivated) {
            Settings.synchronisationManager.addObserver(this);
        }

        Bundle bundle = getArguments();
        selectedBook = (Book) bundle.getSerializable(KEY_ARGUMENT_BOOK);

        if (selectedBook == null) {
            return builder.create();
        }

        Picasso.with(getActivity()).load(selectedBook.getUrls().get(0)).into(imageViewThumbnail);
        textViewTitle.setText(selectedBook.getTitle());
        textViewPublisher.setText(selectedBook.getPublisher());
        textViewShortDescription.setText(selectedBook.getShortDescription());
        textViewShortDescription.setMovementMethod(new ScrollingMovementMethod());
        textViewTitle.setSelected(true);
        builder.setView(view);
        return builder.create();
    }

    @OnClick(R.id.btn_book_detail_play)
    protected void onPlayClick() {
        // Swap the library-fragment with the book-fragment
        if (listener != null) {
            listener.onBookSelected(selectedBook);
            dismiss();
        } else {
            Toast.makeText(getActivity(), "Error playing book", Toast.LENGTH_SHORT).show();
        }

        if(((VideoConnectionActivity) activity).isOnlineModeActivated) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_GO_TO_BOOK, null);
        }
    }

    protected void onPlayInstructionClick() {
        // Swap the library-fragment with the book-fragment
        if (listener != null) {
            listener.onBookSelected(selectedBook);
            dismiss();
        } else {
            Toast.makeText(getActivity(), "Error playing book", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_book_detail_cancel)
    protected void onCancelClick() {
        if(((VideoConnectionActivity) activity).isOnlineModeActivated) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_HIDE_DETAILS, null);
        }
        dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        if(((VideoConnectionActivity) activity).isOnlineModeActivated) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_HIDE_DETAILS, null);
        }
        dismiss();
    }

    protected void onCancelInstructionClick() {
        dismiss();
    }

    @Override
    public void update(Observable o, Object arg) {
        JSONObject object = (JSONObject) arg;
        final String type = Settings.synchronisationManager.getConvertedType(object);

        switch (type) {
            case Settings.INSTRUCTION_HIDE_DETAILS:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onCancelInstructionClick();
                    }
                });
                break;
            case Settings.INSTRUCTION_GO_TO_BOOK:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onPlayInstructionClick();
                    }
                });
                break;
        }
    }
}