package at.fhooe.mc.ba.remotereading.ui.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.data.Book;
import at.fhooe.mc.ba.remotereading.interfaces.BookPreviewListener;
import at.fhooe.mc.ba.remotereading.ui.fragment.BookDetailDialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;


public class BookPreviewAdapter extends RecyclerView.Adapter<BookPreviewAdapter.BookPreviewViewHolder> implements BookPreviewListener {
    private boolean isOnline;
    private ArrayList<Book> bookList;
    private Activity activity;

    public BookPreviewAdapter(@NonNull Activity activity, @NonNull ArrayList<Book> bookList, boolean isOnline) {
        this.activity = activity;
        this.bookList = bookList;
        this.isOnline = isOnline;
    }

    @Override
    public BookPreviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_preview_list_item, parent, false);

        return new BookPreviewViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(final BookPreviewViewHolder holder, int position) {
        Book book = bookList.get(position);
        holder.textViewTitle.setText(book.getTitle());
        Picasso.with(activity).load(book.getUrls().get(0)).into(holder.imageViewThumbnail);
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    @Override
    public void onBookPreviewClick(int position) {
        BookDetailDialogFragment dialog = new BookDetailDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BookDetailDialogFragment.KEY_ARGUMENT_BOOK, bookList.get(position));
        dialog.setArguments(bundle);
        dialog.show(activity.getFragmentManager(), BookDetailDialogFragment.class.getName());

        if(isOnline) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_SHOW_DETAILS, String.valueOf(position));
        }
    }

    public void onBookPreviewInstructionClick(int position) {
        BookDetailDialogFragment dialog = new BookDetailDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BookDetailDialogFragment.KEY_ARGUMENT_BOOK, bookList.get(position));
        dialog.setArguments(bundle);
        dialog.show(activity.getFragmentManager(), BookDetailDialogFragment.class.getName());
    }

    class BookPreviewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.image_book_preview_item_thumbnail)
        ImageView imageViewThumbnail;

        @BindView(R.id.txt_book_preview_item_title)
        TextView textViewTitle;

        private BookPreviewListener listener;
        BookPreviewViewHolder(View itemView, BookPreviewListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View view) {
            listener.onBookPreviewClick(getAdapterPosition());
        }
    }
}
