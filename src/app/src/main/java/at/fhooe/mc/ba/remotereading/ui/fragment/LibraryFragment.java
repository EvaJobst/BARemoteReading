package at.fhooe.mc.ba.remotereading.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.gestures.BookObserver;
import at.fhooe.mc.ba.remotereading.service.BookServiceImpl;
import at.fhooe.mc.ba.remotereading.ui.GridAutofitLayoutManager;
import at.fhooe.mc.ba.remotereading.ui.activity.VideoConnectionActivity;
import at.fhooe.mc.ba.remotereading.ui.adapter.BookPreviewAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class LibraryFragment extends Fragment implements Observer {

    @BindView(R.id.recyclerview_book_preview)
    protected RecyclerView recyclerViewBookPreview;

    private BookPreviewAdapter bookPreviewAdapter;
    private Activity activity;

    public LibraryFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_library, container, false);
        ButterKnife.bind(this, view);

        activity = getActivity();

        if (Settings.synchronisationManager != null) {
            Settings.synchronisationManager.addObserver(this);
        }

        boolean isOnline = ((VideoConnectionActivity) activity).isOnlineModeActivated;
        BookServiceImpl bookService = new BookServiceImpl();
        bookPreviewAdapter = new BookPreviewAdapter(getActivity(), new ArrayList<>(bookService.getAllAvailableBooks()), isOnline);
        recyclerViewBookPreview.setItemAnimator(new DefaultItemAnimator());
        recyclerViewBookPreview.setLayoutManager(new GridAutofitLayoutManager(getActivity(), 400));
        recyclerViewBookPreview.setAdapter(bookPreviewAdapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void update(Observable o, Object arg) {
        JSONObject object = (JSONObject) arg;
        final String type = Settings.synchronisationManager.getConvertedType(object);
        final String instruction = Settings.synchronisationManager.getConvertedInstruction(object);

        switch (type) {
            case Settings.INSTRUCTION_SHOW_DETAILS:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bookPreviewAdapter.onBookPreviewInstructionClick(Integer.parseInt(instruction));
                    }
                });
                break;
        }
    }
}
