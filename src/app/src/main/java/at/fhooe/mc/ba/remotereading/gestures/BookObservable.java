package at.fhooe.mc.ba.remotereading.gestures;

import android.view.MotionEvent;

/**
 * Created by Eva on 19.02.2018.
 */

public interface BookObservable {
    void addBookObserver(BookObserver bookObserver);
    void removeBookObserver(BookObserver bookObserver);
    void notifyBookObserver(Object object);
}
