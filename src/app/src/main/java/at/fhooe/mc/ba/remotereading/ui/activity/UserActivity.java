package at.fhooe.mc.ba.remotereading.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.UUID;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.UserHelper;
import at.fhooe.mc.ba.remotereading.data.User;
import at.fhooe.mc.ba.remotereading.ui.adapter.UserListAdapter;
import at.fhooe.mc.ba.remotereading.ui.decoration.HorizontalDividerItemDecoration;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserActivity extends AppCompatActivity implements UserListAdapter.OnUserSelectedListener {

    public final static String EXTRA_DEEP_LINK_DATA = "extraDeepLinkData";

    @BindView(R.id.textinput_new_user)
    protected TextInputLayout tilNewUser;

    @BindView(R.id.edt_new_user)
    protected EditText edtNewUser;

    @BindView(R.id.toolbar_user)
    protected Toolbar toolbar;

    @BindView(R.id.recyclerview_users)
    protected RecyclerView recyclerViewUsers;
    private UserListAdapter userListAdapter;
    private Uri deepLinkData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            deepLinkData = data;
        }

        ArrayList<User> users = new ArrayList<>();
        users.addAll(Settings.getUserList(this));

        userListAdapter = new UserListAdapter(this, users);
        recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewUsers.addItemDecoration(new HorizontalDividerItemDecoration(this));
        recyclerViewUsers.setAdapter(userListAdapter);
        recyclerViewUsers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        checkPermissions();
    }

    @OnClick(R.id.btn_add_user)
    protected void onClickAddUser() {
        if (!isUsernameValid()) {
            tilNewUser.setErrorEnabled(true);
            tilNewUser.setError(getResources().getString(R.string.error_invalid_username));
            return;
        }

        tilNewUser.setErrorEnabled(false);

        User user = new User(edtNewUser.getText().toString(), UUID.randomUUID().toString());
        UserHelper.addUser(this, user);
        userListAdapter.addUser(user);

        edtNewUser.setText("");
    }

    private void checkPermissions() {
        String[] permissions = {
                "android.permission.CAMERA",
                "android.permission.RECORD_AUDIO",
                "android.permission.INTERNET",
                "android.permission.ACCESS_NETWORK_STATE",
                "android.permission.MODIFY_AUDIO_SETTINGS",
                "android.permission.WAKE_LOCK"
        };

        ActivityCompat.requestPermissions(this, permissions, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onUserSelected(User user) {
        Intent intent = new Intent(this, LobbyActivity.class);
        Settings.currentActiveUser = user;
        if (deepLinkData != null) {
            intent.putExtra(EXTRA_DEEP_LINK_DATA, deepLinkData.toString());
        }
        startActivity(intent);
        deepLinkData = null;
    }

    private boolean isUsernameValid() {
        String name = edtNewUser.getText().toString();
        return name.trim().length() > 0 && !name.contains("&") && !name.contains("?");
    }
}
