package at.fhooe.mc.ba.remotereading.data;

import java.io.Serializable;
import java.util.List;

public class Book implements Serializable {
    private String title;
    private int minimumAge;
    private String language;
    private String publisher;
    private String publisherLink;
    private List<Author> authorList;
    private int publishingYear;
    private String isbn;
    private List<String> reviewLinks;
    private String shopLink;
    private List<String> awards;
    private String shortDescription;
    private List<String> urls;

    public Book(String title,
                int minimumAge,
                String language,
                String publisher,
                String publisherLink,
                List<Author> authorList,
                int publishingYear,
                String isbn,
                List<String> reviewLinks,
                String shopLink,
                List<String> awards,
                String shortDescription,
                List<String> urls) {
        this.title = title;
        this.minimumAge = minimumAge;
        this.language = language;
        this.publisher = publisher;
        this.publisherLink = publisherLink;
        this.authorList = authorList;
        this.publishingYear = publishingYear;
        this.isbn = isbn;
        this.reviewLinks = reviewLinks;
        this.shopLink = shopLink;
        this.awards = awards;
        this.shortDescription = shortDescription;
        this.urls = urls;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMinimumAge() {
        return minimumAge;
    }

    public void setMinimumAge(int minimumAge) {
        this.minimumAge = minimumAge;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublisherLink() {
        return publisherLink;
    }

    public void setPublisherLink(String publisherLink) {
        this.publisherLink = publisherLink;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public int getPublishingYear() {
        return publishingYear;
    }

    public void setPublishingYear(int publishingYear) {
        this.publishingYear = publishingYear;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<String> getReviewLinks() {
        return reviewLinks;
    }

    public void setReviewLinks(List<String> reviewLinks) {
        this.reviewLinks = reviewLinks;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public List<String> getAwards() {
        return awards;
    }

    public void setAwards(List<String> awards) {
        this.awards = awards;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrl(List<String> urls) {
        this.urls = urls;
    }
}