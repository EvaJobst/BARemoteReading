package at.fhooe.mc.ba.remotereading.p2p;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.json.JSONException;
import org.json.JSONObject;

import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.p2p.keys.PubnubKeyManager;
import at.fhooe.mc.ba.remotereading.p2p.keys.PubnubKeyProvider;
import at.fhooe.mc.ba.remotereading.ui.activity.VideoConnectionActivity;

public class PubnubManager {
    private static final String STANDBY_SUFFIX = "-stdby";

    private final PubnubKeyProvider keyProvider;
    private Pubnub pubnub;
    private Activity activity = null;
    private final String standByChannel;

    public PubnubManager() {
        keyProvider = new PubnubKeyManager();
        standByChannel = Settings.currentActiveUser.getDisplayName() + STANDBY_SUFFIX;
        pubnub = new Pubnub(keyProvider.getPubKey(), keyProvider.getSubKey());
        pubnub.setUUID(Settings.currentActiveUser.getDisplayName());//Settings.currentActiveUser.getCallNumber());
    }

    /**
     * Initializes a Pubnub object, subscribes it to the standby channel and deals with receiving calls.
     *
     * @param activity Current activity
     */
    public void initPubNub(final Activity activity) {
        this.activity = activity;
        initPubNub();
    }

    public void initPubNub() {
        try {
            pubnub.subscribe(standByChannel, standByChannelCallback);
            /*pubnub.presence(standByChannel, new Callback() {
                @Override
                public void successCallback(String channel, Object message) {
                    JSONObject jsonMessage = null;
                    try {
                        jsonMessage = new JSONObject(message.toString());
                        String action = jsonMessage.get("action").toString();
                        String uuid = jsonMessage.get("uuid").toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    super.successCallback(channel, message);
                }
            });*/
        } catch (PubnubException ex) {
            Log.e(PubnubManager.class.getName(), ex.getMessage());
        }
    }

    public void deinitPubNub() {
        //pubnub.unsubscribePresence(standByChannel);
        pubnub.unsubscribe(standByChannel);
    }

    private Callback standByChannelCallback = new Callback() {
        @Override
        public void successCallback(String channel, Object message) {
            super.successCallback(channel, message);
            if (!(message instanceof JSONObject)) {
                return;
            }

            JSONObject json = (JSONObject) message;

            try {
                // A user has established a connection to us, add him to the current-active users friend-list
                Settings.currentConnectedUser = json.getString(Settings.JSON_CALL_USER);
                Log.i("accept", "username: " + Settings.currentActiveUser.getDisplayName());
                Log.i("accept", "callNumber: " + Settings.currentConnectedUser);
                Intent intent = new Intent(activity, VideoConnectionActivity.class);
                intent.putExtra(Settings.JSON_USERNAME, Settings.currentActiveUser.getDisplayName());
                intent.putExtra(Settings.JSON_HAS_DIALED, false);
                intent.putExtra(Settings.JSON_CALL_USER, Settings.currentConnectedUser);
                intent.putExtra(Settings.JSON_IS_ONLINE_MODE_ACTIVATED, true);
                activity.startActivity(intent);
            } catch (JSONException ex) {
                Log.e(PubnubManager.class.getName(), ex.getMessage());
            }

            Log.i(PubnubManager.class.getName(), "Pubnub-Subscribed-To-Standby: " + message.toString());
        }

        @Override
        public void connectCallback(String channel, Object message) {
            super.connectCallback(channel, message);
            Log.i(PubnubManager.class.getName(), "Pubnub-Connection-Established: " + message.toString());
            setUserStatus(Settings.STATUS_AVAILABLE);
        }

        @Override
        public void errorCallback(String channel, PubnubError error) {
            super.errorCallback(channel, error);
            Log.e(PubnubManager.class.getName(), "Pubnub-Error: " + error);
        }
    };

    private void setUserStatus(String status) {
        try {
            JSONObject state = new JSONObject();
            state.put(Settings.JSON_STATUS, status);
            pubnub.setState(standByChannel, Settings.currentActiveUser.getCallNumber(), state, new Callback() {
                @Override
                public void successCallback(String channel, Object message) {
                    Log.i(PubnubManager.class.getName(), "Pubnub-State-Set: " + message.toString());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls a user
     *
     * @param callNumber is called
     */
    public void dispatchCall(final String callNumber, final boolean dialed) {
        Log.d("Pubnub", "Dispatched call");
        final String callNumStdBy = callNumber + STANDBY_SUFFIX;
        JSONObject jsonCall = new JSONObject();
        try {
            jsonCall.put(Settings.JSON_CALL_USER, Settings.currentActiveUser.getDisplayName());
            pubnub.publish(callNumStdBy, jsonCall, new Callback() {
                @Override
                public void successCallback(String channel, Object message) {
                    Log.d("MA-dCall", "SUCCESS: " + message.toString());
                    Settings.currentConnectedUser = callNumber;
                    Log.i("dispatch", "username: " + Settings.currentActiveUser.getCallNumber());
                    Log.i("dispatch", "callNumber: " + callNumber);
                    Intent intent = new Intent(activity, VideoConnectionActivity.class);
                    intent.putExtra(Settings.JSON_USERNAME, Settings.currentActiveUser.getDisplayName());
                    intent.putExtra(Settings.JSON_HAS_DIALED, dialed);
                    intent.putExtra(Settings.JSON_CALL_USER, callNumber);
                    intent.putExtra(Settings.JSON_IS_ONLINE_MODE_ACTIVATED, true);
                    activity.startActivity(intent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public PubnubKeyProvider getKeyProvider() {
        return keyProvider;
    }
}
