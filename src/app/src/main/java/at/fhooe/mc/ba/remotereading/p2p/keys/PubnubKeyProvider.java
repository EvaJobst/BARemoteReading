package at.fhooe.mc.ba.remotereading.p2p.keys;

public interface PubnubKeyProvider {
    String getPubKey();
    void setPubKey();
    String getSubKey();
    void setSubKey();
}
