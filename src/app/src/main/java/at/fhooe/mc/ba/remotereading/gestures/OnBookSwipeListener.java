package at.fhooe.mc.ba.remotereading.gestures;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Eva on 19.02.2018.
 */

public class OnBookSwipeListener extends GestureDetector.SimpleOnGestureListener implements BookObservable {
    private final int SWIPE_THRESHOLD = 300;
    float previousScaleFactor;
    ArrayList<BookObserver> bookObserverList;

    public OnBookSwipeListener() {
        bookObserverList = new ArrayList<>();
        previousScaleFactor = 1.0f;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if(e1.getX() - e2.getX() > SWIPE_THRESHOLD) {
            notifyBookObserver(true);
            return true;
        }

        if(e2.getX() - e1.getX() > SWIPE_THRESHOLD) {
            notifyBookObserver(false);
            return true;
        }

        return false;
    }

    @Override
    public void addBookObserver(BookObserver bookObserver) {
        bookObserverList.add(bookObserver);
    }

    @Override
    public void removeBookObserver(BookObserver bookObserver) {
        bookObserverList.remove(bookObserver);
    }

    @Override
    public void notifyBookObserver(Object object) {
        if (object instanceof Boolean) {
            for (BookObserver bookObserver : bookObserverList) {
                bookObserver.notifyBookObserver((Boolean) object);
            }
        }
    }
}
