package at.fhooe.mc.ba.remotereading;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import at.fhooe.mc.ba.remotereading.data.User;
import at.fhooe.mc.ba.remotereading.p2p.PubnubManager;
import at.fhooe.mc.ba.remotereading.p2p.SynchronisationManager;

public class Settings {
    public static SynchronisationManager synchronisationManager;
    public static PubnubManager pubnubManager;

    public static User currentActiveUser = null;
    public static String currentConnectedUser = null;

    public static final String JSON_IS_ONLINE_MODE_ACTIVATED = "isOnlineModeActivated";
    public static final String JSON_USERNAME = "user_name";
    public static final String JSON_CALL_USER = "call_user";
    public static final String JSON_HAS_DIALED = "has_dialed";
    public static final String JSON_CALL_TIME = "call_time";
    public static final String JSON_OCCUPANCY = "occupancy";
    public static final String JSON_STATUS    = "status";

    public static final String JSON_USER_MSG  = "user_message";
    public static final String JSON_MSG_UUID  = "msg_uuid";
    public static final String JSON_MSG       = "msg_message";
    public static final String JSON_TIME      = "msg_timestamp";

    public static final String JSON_INSTRUCTION_TYPE = "command_type";
    public static final String JSON_INSTRUCTION = "command";

    public static final String STATUS_AVAILABLE = "Available";
    public static final String STATUS_OFFLINE   = "Offline";
    public static final String STATUS_BUSY      = "Busy";

    // Instruction Types
    public static final String INSTRUCTION_SHOW_DETAILS = "show_details";
    public static final String INSTRUCTION_HIDE_DETAILS = "hide_details";
    public static final String INSTRUCTION_NEXT_PAGE = "next_page";
    public static final String INSTRUCTION_PREVIOUS_PAGE = "previous_page";
    public static final String INSTRUCTION_GO_TO_BOOK = "go_to_book";
    public static final String INSTRUCTION_BACK_TO_LIBRARY = "back_to_library";
    public static final String INSTRUCTION_SHARE_IDENTITY = "share_identity";
    public static final String INSTRUCTION_HANGUP = "hangup";

    /* Shared Preferences Keys */
    public static final String KEY_USER_LIST = "keyUserList";

    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    // User-List

    public static void setUserList(final Context context, List<User> userList) {
        Gson gson = new Gson();
        getSharedPreferences(context).edit().putString(KEY_USER_LIST, gson.toJson(userList)).apply();
    }

    public static List<User> getUserList(final Context context) {
        String json = getSharedPreferences(context).getString(KEY_USER_LIST, null);
        if (json == null) {
            return new ArrayList<>();
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<User>>(){}.getType();
        return gson.fromJson(json, type);
    }
}
