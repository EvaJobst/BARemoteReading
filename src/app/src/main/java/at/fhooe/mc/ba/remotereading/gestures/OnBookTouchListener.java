package at.fhooe.mc.ba.remotereading.gestures;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import at.fhooe.mc.ba.remotereading.R;

/**
 * Created by Eva on 19.02.2018.
 */

public class OnBookTouchListener extends GestureDetector.SimpleOnGestureListener implements BookObservable {
    ArrayList<BookObserver> bookObserverList;

    public OnBookTouchListener() {
        bookObserverList = new ArrayList<>();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        notifyBookObserver(new float[] {e.getX(), e.getY()});
        return true;
    }

    @Override
    public void addBookObserver(BookObserver bookObserver) {
        bookObserverList.add(bookObserver);
    }

    @Override
    public void removeBookObserver(BookObserver bookObserver) {
        bookObserverList.remove(bookObserver);
    }

    @Override
    public void notifyBookObserver(Object object) {
        if (object instanceof float[]) {
            float[] position = (float[])object;
            for (BookObserver bookObserver : bookObserverList) {
                bookObserver.notifyBookObserver(position[0], position[1]);
            }
        }
    }
}
