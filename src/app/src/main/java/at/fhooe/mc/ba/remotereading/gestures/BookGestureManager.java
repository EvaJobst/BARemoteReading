package at.fhooe.mc.ba.remotereading.gestures;

import android.app.Activity;
import android.support.v4.view.GestureDetectorCompat;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.ui.fragment.BookFragment;

/**
 * Created by Eva on 09.02.2018.
 */

public class BookGestureManager {
    OnBookTouchListener onBookTouchListener;
    OnBookSwipeListener onBookSwipeListener;
    OnBookPinchListener onBookPinchListener;

    public BookGestureManager(BookFragment fragment) {
        // Initialization
        onBookTouchListener = new OnBookTouchListener();
        onBookSwipeListener = new OnBookSwipeListener();
        onBookPinchListener = new OnBookPinchListener();

        onBookTouchListener.addBookObserver(fragment);
        onBookSwipeListener.addBookObserver(fragment);
        onBookPinchListener.addBookObserver(fragment);

        // Load Gesture detection
        final GestureDetectorCompat touchGestureDetector = new GestureDetectorCompat(fragment.getActivity(), onBookTouchListener);
        final GestureDetectorCompat pinchGestureDetector = new GestureDetectorCompat(fragment.getActivity(), onBookSwipeListener);
        final ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(fragment.getActivity(), onBookPinchListener);

        fragment.getView().findViewById(R.id.image_book_page).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                touchGestureDetector.onTouchEvent(motionEvent);
                pinchGestureDetector.onTouchEvent(motionEvent);
                scaleGestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
    }
}
