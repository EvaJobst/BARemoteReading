package at.fhooe.mc.ba.remotereading.p2p;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import at.fhooe.mc.ba.remotereading.Settings;
import me.kevingleason.pnwebrtc.PnRTCClient;

public class SynchronisationManager extends Observable {
    private PnRTCClient pnRTCClient;
    private Vector<Observer> observerList = new Vector<>();

    public SynchronisationManager(PnRTCClient client) {
        pnRTCClient = client;
    }

    public void send(String type, String instruction){
        if(pnRTCClient != null) {
            JSONObject msgJson = new JSONObject();
            try {
                msgJson.put(Settings.JSON_INSTRUCTION_TYPE, type);
                msgJson.put(Settings.JSON_INSTRUCTION, instruction);

                Log.d("TRANSITTING DATA", "Sending: " + msgJson.toString());
                Log.d("SENDING MESSAGE TO", Settings.currentConnectedUser);
                pnRTCClient.transmit(Settings.currentConnectedUser, msgJson);
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public String getConvertedType(JSONObject object) {
        try {
            return object.getString(Settings.JSON_INSTRUCTION_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getConvertedInstruction(JSONObject object) {
        if (object.has(Settings.JSON_INSTRUCTION)) {
            try {
                return object.getString(Settings.JSON_INSTRUCTION);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public synchronized void addObserver(Observer o) {
        if(pnRTCClient != null) {
            observerList.add(o);
        }
    }

    @Override
    public void notifyObservers(Object arg) {
        for(Observer o : observerList) {
            o.update(this, arg);
        }
    }

    @Override
    public synchronized void deleteObserver(Observer o) {
        observerList.remove(o);
    }
}
