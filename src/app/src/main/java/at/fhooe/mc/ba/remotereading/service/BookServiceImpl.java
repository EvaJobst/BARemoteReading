package at.fhooe.mc.ba.remotereading.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import at.fhooe.mc.ba.remotereading.data.Author;
import at.fhooe.mc.ba.remotereading.data.Book;

public class BookServiceImpl implements BookService {
    @Override
    public Book getBook(int id) {
        return null;
    }

    @Override
    public Book getBook(String title) {
        return null;
    }

    @Override
    public Collection<Book> getAllAvailableBooks() {
        Collection<Book> books = new ArrayList<>();

        books.add(getFoxyPlaysATrick());
        books.add(getGaggalagu());
        books.add(getTraumfee());
        books.add(getTroedltrudl());

        books.add(getFoxyPlaysATrick());
        books.add(getGaggalagu());
        books.add(getTraumfee());
        books.add(getTroedltrudl());

        books.add(getFoxyPlaysATrick());
        books.add(getGaggalagu());
        books.add(getTraumfee());
        books.add(getTroedltrudl());

        books.add(getFoxyPlaysATrick());
        books.add(getGaggalagu());
        books.add(getTraumfee());
        books.add(getTroedltrudl());
        return books;
    }

    private Book getFoxyPlaysATrick() {
        List<Author> authors = new ArrayList<>();
        authors.add(new Author("Nathida", "Esmail", "A"));
        authors.add(new Author("Mdu", "Ntuli", "I"));
        authors.add(new Author("Rice", "Samantha", ""));

        List<String> reviews = new ArrayList<>();
        List<String> awards = new ArrayList<>();
        List<String> urls = new ArrayList<>();

        for(int i = 1; i <= 12; i++) {
            urls.add(getURL("http://vorlesen-verbindet.com/wp-content/uploads/sites/3/2017/03/Foxy-Joxy-plays-a-trick_bookdash_", ".jpg", i));
        }

        String summary = "Foxy Joxy, a sly fox, sells watermelons. One day he has a clever idea. The animals were not happy. What will they do to teach him a lesson?";

        return new Book("Foxy Plays A Trick", 5, "EN", "BookDash.org",
                "http://bookdash.org/", authors, 2017, "978-1-928377-31-3",
                reviews, "", awards, summary, urls);
    }

    private Book getGaggalagu() {
        List<Author> authors = new ArrayList<>();
        authors.add(new Author("Michael", "Stavaric", "A"));
        authors.add(new Author("Renate", "Habinger", "I"));

        List<String> reviews = new ArrayList<>();
        List<String> awards = new ArrayList<>();
        awards.add("Österreichischen Kinder- und Jugendbuchpreis 2007");

        List<String> urls = new ArrayList<>();

        for(int i = 1; i <= 26; i++) {
            urls.add(getURL("http://vorlesen-verbindet.com/wp-content/uploads/sites/3/2017/02/Gaggalagu_kookbooks_", ".jpg", i));
        }

        String summary = "So vielfältig die unterschiedlichen Sprachen sind," +
                "so unterschiedlich sind die Weisen, wie in ihnen Tierlaute wiedergegeben werden. Aber heißt das auch," +
                "dass die Tiere sich untereinander nicht verstehen? Und die Menschen? Und wie kommen sie ins Buch? Ja, wann ist ein Hahn ein Hahn?\n" +
                "Mit Witz, Weltgewandtheit und feinem sprachlichem und zeichnerischem Hintersinn führen Michael Stavaric und Renate Habinger durch" +
                "die Klippen tierischer und menschlicher Kommunikation. Seine funkelnden Geschichten und Gedichte und ihre Zeichnungen voller hinreißender" +
                "Details erzählen von Unterschieden und Gemeinsamkeiten und davon, dass es nicht immer nur darauf ankommt, dieselbe Sprache zu sprechen, um einander zu verstehen.";

        return new Book("Gaggalagu", 8, "DE", "kookbooks",
                "http://www.kookbooks.de/", authors, 2006, "978-3-937445-21-2",
                reviews, "", awards, summary, urls);
    }

    private Book getTraumfee() {
        List<Author> authors = new ArrayList<>();
        authors.add(new Author("Petra", "Postert", "A"));
        authors.add(new Author("Nina", "Dulleck", "I"));

        List<String> reviews = new ArrayList<>();
        reviews.add("http://www.tulipan-verlag.de/Buecher/Traumfee-Lula-und-die-Zahnfee-Margarete.html");

        List<String> awards = new ArrayList<>();
        List<String> urls = new ArrayList<>();

        for(int i = 1; i <= 24; i++) {
            urls.add(getURL("http://vorlesen-verbindet.com/wp-content/uploads/sites/3/2016/11/Traumfee-Lula_Tulipan-Verlag_", ".jpg", i));
        }

        String summary = "Traumfee Lula entdeckt beim Träumeflüstern eine Schachtel auf dem Nachttisch. Ob das Ding darin wohl ein glitzernder Ring ist? Kater Dschango weiß es besser: Das ist ein Milchzahn für die Zahnfee. »Aber Zahnfeen gibt’s doch nicht!«, denkt Lula. Da taucht plötzlich eine fremde Fee auf ...";

        return new Book("Traumfee Lula und die Zahnfee Margarete", 7, "DE", "Tulipan Verlag",
                "http://www.tulipan-verlag.de/", authors, 2014, "978-3-8642-9204-0",
                reviews, "", awards, summary, urls);
    }

    private Book getTroedltrudl() {
        List<Author> authors = new ArrayList<>();
        authors.add(new Author("Renate", "Stockreiter", "A"));

        List<String> reviews = new ArrayList<>();
        reviews.add("http://www.biblio.at/rezonline/ajax.php?action=rezension&medid=132962&rezid=43640");
        List<String> awards = new ArrayList<>();
        awards.add("2013: LeserStimmen | Kollektion");

        List<String> urls = new ArrayList<>();

        for(int i = 1; i <= 17; i++) {
            urls.add(getURL("http://vorlesen-verbindet.com/wp-content/uploads/sites/3/2016/11/Tr%C3%B6dltrudls-Klippklapptraum_Tyrolia-Verlag_", ".jpg", i));
        }

        String summary = "Trudl soll die Wäsche von der Leine holen, die zwischen alten Apfelbäumen zickzack über die Wiese gespannt ist. Doch das ist gar nicht so leicht, denn diese Aufgabe wird schwuppdiwupp zu einem kleinen Abenteuer mit grimmigen Wäschekluppen und einem kitzelnden Zwerg. Kein Wunder, dass es so wieder einmal ein bisschen länger dauert ...";

        return new Book("Trödltrudls Klippklapptraum", 6, "DE", "Tyrolia",
                "http://www.tyroliaverlag.at/", authors, 2012, "978-3-7022-3377-8",
                reviews, "", awards, summary, urls);
    }

    private String getURL(String prefix, String suffix, int page) {
        if(page < 10) {
            return prefix + "0" + String.valueOf(page) + suffix;
        }

        else {
            return prefix + String.valueOf(page) + suffix;
        }
    }
}