package at.fhooe.mc.ba.remotereading.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.data.Book;
import at.fhooe.mc.ba.remotereading.gestures.BookGestureManager;
import at.fhooe.mc.ba.remotereading.gestures.BookObserver;
import at.fhooe.mc.ba.remotereading.ui.activity.VideoConnectionActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookFragment extends Fragment implements Observer, BookObserver {

    public static final String KEY_ARGUMENT_BOOK = "keyArgumentBook";

    @BindView(R.id.image_book_page)
    public ImageView imageViewBookPage;

    private Book selectedBook;
    private int currentPageIndex = 0;
    private Activity activity;

    public BookFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book, container, false);
        ButterKnife.bind(this, view);

        activity = getActivity();

        if(((VideoConnectionActivity) activity).isOnlineModeActivated) {
            Settings.synchronisationManager.addObserver(this);
        }

        /*if (activity != null && activity instanceof VideoConnectionActivity) {
            ((VideoConnectionActivity) activity).menu.getItem(1).setVisible(true);
        }*/

        Bundle bundle = getArguments();
        selectedBook = (Book) bundle.getSerializable(KEY_ARGUMENT_BOOK);

        if (selectedBook == null) {
            return view;
        }

        Picasso.with(activity).load(selectedBook.getUrls().get(currentPageIndex)).into(imageViewBookPage);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        new BookGestureManager(this);
    }

    @OnClick({R.id.btn_book_fragment_next_page /*, R.id.btn_invisible_next_page*/})
    public void onNextPageClick() {
        if (currentPageIndex == selectedBook.getUrls().size() - 1) {
            return;
        }
        currentPageIndex++;
        Picasso.with(getActivity()).load(selectedBook.getUrls().get(currentPageIndex)).into(imageViewBookPage);

        if(((VideoConnectionActivity)activity).isOnlineModeActivated) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_NEXT_PAGE, null);
        }
    }

    protected void onNextPageInstructionClick() {
        if (currentPageIndex == selectedBook.getUrls().size() - 1) {
            return;
        }
        currentPageIndex++;
        Picasso.with(getActivity()).load(selectedBook.getUrls().get(currentPageIndex)).into(imageViewBookPage);
    }

    @OnClick({R.id.btn_book_fragment_previous_page /*, R.id.btn_invisible_previous_page*/})
    public void onPreviousPageClick() {
        if (currentPageIndex == 0) {
            return;
        }
        currentPageIndex--;
        Picasso.with(getActivity()).load(selectedBook.getUrls().get(currentPageIndex)).into(imageViewBookPage);

        if(((VideoConnectionActivity)activity).isOnlineModeActivated) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_PREVIOUS_PAGE, null);
        }
    }

    protected void onPreviousPageInstructionClick() {
        if (currentPageIndex == 0) {
            return;
        }
        currentPageIndex--;
        Picasso.with(getActivity()).load(selectedBook.getUrls().get(currentPageIndex)).into(imageViewBookPage);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void update(Observable o, Object arg) {
        JSONObject object = (JSONObject) arg;
        final String type = Settings.synchronisationManager.getConvertedType(object);

        switch (type) {
            case Settings.INSTRUCTION_NEXT_PAGE:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onNextPageInstructionClick();
                    }
                });
                break;
            case Settings.INSTRUCTION_PREVIOUS_PAGE:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onPreviousPageInstructionClick();
                    }
                });
                break;
        }
    }

    @Override
    public void notifyBookObserver(float x, float y) {
        final ImageView imageView = new ImageView(activity);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imageView.setImageResource(R.drawable.ic_fiber_manual_record_black_24dp);
        imageView.setX(x);
        imageView.setY(y);

        final RelativeLayout relativeLayout = getView().findViewById(R.id.fragment_book_view);
        relativeLayout.addView(imageView);

        Timer t = new Timer(false);
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        relativeLayout.removeView(imageView);
                    }
                });
            }
        }, 2000);
    }

    @Override
    public void notifyBookObserver(float scale) {
        imageViewBookPage.setScaleX(scale);
        imageViewBookPage.setScaleY(scale);
    }

    @Override
    public void notifyBookObserver(boolean isLeftSwipe) {
        if (isLeftSwipe) {
            onNextPageClick();
        }

        else {
            onPreviousPageClick();
        }
    }
}
