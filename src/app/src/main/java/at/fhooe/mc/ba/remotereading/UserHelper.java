package at.fhooe.mc.ba.remotereading;


import android.content.Context;
import android.util.Log;

import java.util.List;

import at.fhooe.mc.ba.remotereading.data.User;

public class UserHelper {
    public static void addUser(Context context, User user) {
        List<User> userList = Settings.getUserList(context);
        userList.add(user);
        Settings.setUserList(context, userList);
    }

    public static void addFriendToCurrentUser(final Context context, User friend) {
        // Check if the user has already got this friend
        for (User u : Settings.currentActiveUser.getFriendList()) {
            if (u.equals(friend)) {
                return;
            }
        }

        // Iterate through the user-list, add the friend to the passed user and persist it
        List<User> userList = Settings.getUserList(context);
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).equals(Settings.currentActiveUser)) {
                userList.get(i).getFriendList().add(friend);
                Settings.currentActiveUser = userList.get(i);
                Settings.setUserList(context, userList);
                return;
            }
        }
    }

    /**
     * Extracts a user from a deep-link
     * @param deepLink The link from where the user will be extracted
     * @return A user extracted from a deep-link
     */
    public static User getUserFromDeepLink(String deepLink) {
        String[] parts = deepLink.split("[&?]");
        if (parts.length == 3) {
            return new User(parts[1], parts[2]);
        }

        Log.e(UserHelper.class.getName(), "Error getting user from deep-link");
        return null;
    }

    public static String getDeepLinkUrlFromUser(User user) {
        return "http://www.vorlesen-verbindet.com/connect?" + user.getDisplayName() + "&" + user.getUuid();
    }
}
