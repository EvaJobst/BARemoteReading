package at.fhooe.mc.ba.remotereading.interfaces;

public interface BookPreviewListener {
    void onBookPreviewClick(int position);
}
