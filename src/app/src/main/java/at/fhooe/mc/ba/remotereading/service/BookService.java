package at.fhooe.mc.ba.remotereading.service;


import java.util.Collection;

import at.fhooe.mc.ba.remotereading.data.Book;

interface BookService {
    Book getBook(int id);
    Book getBook(String title);
    Collection<Book> getAllAvailableBooks();
}
