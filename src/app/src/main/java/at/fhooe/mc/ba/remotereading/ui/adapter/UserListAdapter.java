package at.fhooe.mc.ba.remotereading.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.data.User;
import at.fhooe.mc.ba.remotereading.interfaces.UserClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> implements UserClickListener {

    private ArrayList<User> userList;
    private OnUserSelectedListener onUserSelectedListener;

    public UserListAdapter(OnUserSelectedListener onUserSelectedListener, ArrayList<User> userList) {
        this.onUserSelectedListener = onUserSelectedListener;
        this.userList = userList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        return new UserViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.name.setText(userList.get(position).getDisplayName());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public void onUserClick(int position) {
        onUserSelectedListener.onUserSelected(userList.get(position));
    }

    class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_user_list_item_name)
        public TextView name;

        private UserClickListener listener;

        UserViewHolder(@NonNull View itemView, @NonNull UserClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View view) {
            listener.onUserClick(getAdapterPosition());
        }
    }

    public interface OnUserSelectedListener {
        void onUserSelected(User user);
    }

    public boolean contains(String s) {
        return userList.contains(s);
    }

    public void addUser(@NonNull User user) {
        userList.add(user);
        notifyDataSetChanged();
    }

    public void updateUserList(List<User> userList) {
        this.userList.clear();
        this.userList.addAll(userList);
        notifyDataSetChanged();
    }
}