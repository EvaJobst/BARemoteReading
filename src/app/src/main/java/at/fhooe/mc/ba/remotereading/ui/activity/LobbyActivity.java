package at.fhooe.mc.ba.remotereading.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;


import java.util.ArrayList;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.UserHelper;
import at.fhooe.mc.ba.remotereading.data.User;
import at.fhooe.mc.ba.remotereading.p2p.PubnubManager;
import at.fhooe.mc.ba.remotereading.ui.adapter.UserListAdapter;
import at.fhooe.mc.ba.remotereading.ui.decoration.HorizontalDividerItemDecoration;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LobbyActivity extends AppCompatActivity implements UserListAdapter.OnUserSelectedListener {
    @BindView(R.id.btn_lobby_share)
    protected Button btnShare;

    @BindView(R.id.rv_lobby)
    protected RecyclerView recyclerViewCallUsers;
    private UserListAdapter userListAdapter;

    private PubnubManager pubnubManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        // Initialize the Pubnubmanager
        pubnubManager = new PubnubManager();
        pubnubManager.initPubNub(this);
        Settings.pubnubManager = pubnubManager;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(UserActivity.EXTRA_DEEP_LINK_DATA)) {
            // We accepted a call via deep-linking, so call the user
            if (pubnubManager != null) {
                User callUser = UserHelper.getUserFromDeepLink(bundle.getString(UserActivity.EXTRA_DEEP_LINK_DATA));

                if (callUser != null) {
                    pubnubManager.dispatchCall(callUser.getDisplayName(), true);
                }
            }
        }

        ArrayList<User> friendList = new ArrayList<>();
        friendList.addAll(Settings.currentActiveUser.getFriendList());

        userListAdapter = new UserListAdapter(this, friendList);
        recyclerViewCallUsers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCallUsers.addItemDecoration(new HorizontalDividerItemDecoration(this));
        recyclerViewCallUsers.setAdapter(userListAdapter);
        recyclerViewCallUsers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected void onResume() {
        super.onResume();
        pubnubManager.initPubNub();
        userListAdapter.updateUserList(Settings.currentActiveUser.getFriendList());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pubnubManager.deinitPubNub();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //pubnubManager.deinitPubNub();
    }

    @OnClick(R.id.btn_lobby_share)
    protected void onShareClicked() {
        Intent sendIntent = new Intent();
        String shareText = "There is a new invitation from " +
                Settings.currentActiveUser.getDisplayName() +
                ", accept by clicking " + UserHelper.getDeepLinkUrlFromUser(Settings.currentActiveUser);
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_intent_subject));
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        Intent choser = Intent.createChooser(sendIntent, "Invite a relative:");
        startActivity(choser);
    }

    @OnClick(R.id.btn_lobby_start_local)
    protected void onStartLocalClicked() {
        Intent intent = new Intent(this, VideoConnectionActivity.class);
        intent.putExtra(Settings.JSON_IS_ONLINE_MODE_ACTIVATED, false);
        startActivity(intent);
    }

    @Override
    public void onUserSelected(User user) {
        if (pubnubManager != null) {
            pubnubManager.dispatchCall(user.getDisplayName(), true);
        }
    }
}