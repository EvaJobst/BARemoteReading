package at.fhooe.mc.ba.remotereading.interfaces;

import at.fhooe.mc.ba.remotereading.data.Book;

public interface BookDetailDialogFragmentListener {
    void onBookSelected(Book book);
}
