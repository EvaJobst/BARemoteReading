package at.fhooe.mc.ba.remotereading.data;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String displayName;
    private String uuid;
    private List<User> friendList;

    public User(String displayName, String uuid) {
        this.displayName = displayName;
        this.uuid = uuid;
        friendList = new ArrayList<>();
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUuid() {
        return uuid;
    }

    public List<User> getFriendList() {
        return friendList;
    }

    public String getCallNumber() {
        return displayName + uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        User rhs = (User) obj;

        return rhs.displayName.equals(this.displayName) && rhs.uuid.equals(this.uuid);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + displayName.hashCode();
        result = 31 * result + uuid.hashCode();
        return result;
    }
}
