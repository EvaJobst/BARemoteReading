package at.fhooe.mc.ba.remotereading.ui.activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.Observable;
import java.util.Observer;

import at.fhooe.mc.ba.remotereading.R;
import at.fhooe.mc.ba.remotereading.Settings;
import at.fhooe.mc.ba.remotereading.UserHelper;
import at.fhooe.mc.ba.remotereading.data.Book;
import at.fhooe.mc.ba.remotereading.data.User;
import at.fhooe.mc.ba.remotereading.interfaces.BookDetailDialogFragmentListener;
import at.fhooe.mc.ba.remotereading.p2p.PubnubManager;
import at.fhooe.mc.ba.remotereading.p2p.SynchronisationManager;
import at.fhooe.mc.ba.remotereading.ui.fragment.BookFragment;
import at.fhooe.mc.ba.remotereading.ui.fragment.LibraryFragment;
import me.kevingleason.pnwebrtc.PnPeer;
import me.kevingleason.pnwebrtc.PnRTCClient;
import me.kevingleason.pnwebrtc.PnRTCListener;

public class VideoConnectionActivity extends AppCompatActivity implements
        BookDetailDialogFragmentListener,
        Observer {
    public static final String VIDEO_TRACK_ID = "videoPN";
    public static final String AUDIO_TRACK_ID = "audioPN";
    public static final String LOCAL_MEDIA_STREAM_ID = "localStreamPN";

    private PubnubManager pubnubManager = new PubnubManager();
    private PnRTCClient pnRTCClient;
    protected GLSurfaceView glSurfaceVideoView;

    private VideoSource localVideoSource;
    private VideoRenderer.Callbacks localRender;
    private VideoRenderer.Callbacks remoteRender;

    public Menu menu;
    public String toolbarText;
    public boolean isOnlineModeActivated = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_connection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarText = getSupportActionBar().getTitle().toString();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        final Bundle extras = getIntent().getExtras();

        // The view on which we are going to render the video's
        glSurfaceVideoView = (GLSurfaceView) findViewById(R.id.gl_surface);

        // Check if the activity was started in online-mode, if it was not started in
        // online mode we do not have to show and set up the open-gl surfaces
        if (!extras.containsKey(Settings.JSON_IS_ONLINE_MODE_ACTIVATED) || !extras.getBoolean(Settings.JSON_IS_ONLINE_MODE_ACTIVATED)) {
            switchToOfflineMode();
        } else {
            toolbarText = toolbarText + " | Connected to: " + Settings.currentConnectedUser;
            setupVideoInterface();
            Settings.synchronisationManager = new SynchronisationManager(pnRTCClient);
            Settings.synchronisationManager.addObserver(this);

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Gson gson = new Gson();
                    Settings.synchronisationManager.send(Settings.INSTRUCTION_SHARE_IDENTITY, gson.toJson(Settings.currentActiveUser));

                    return null;
                }
            }.execute();
        }

        getSupportActionBar().setTitle(toolbarText);

        // Load the fragment-container with the library-fragment
        Fragment libraryFragment = new LibraryFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, libraryFragment, LibraryFragment.class.getName());
        transaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isOnlineModeActivated) {
            this.glSurfaceVideoView.onPause();
            Settings.synchronisationManager.send(Settings.INSTRUCTION_HANGUP, null);
            hangup();
        }

        /*pubnubManager.deinitPubNub();*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        //pubnubManager.initPubNub();
        if (isOnlineModeActivated) {
            this.glSurfaceVideoView.onResume();
            this.localVideoSource.restart();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isOnlineModeActivated) {
            Settings.synchronisationManager.send(Settings.INSTRUCTION_HANGUP, null);
            hangup();
        }

        //pubnubManager.deinitPubNub();
    }

    @Override
    public void onBookSelected(Book book) {
        final String title = book.getTitle();
        Log.i(VideoConnectionActivity.class.getName(), book.getTitle());
        Fragment bookFragment = new BookFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BookFragment.KEY_ARGUMENT_BOOK, book);
        bookFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, bookFragment, BookFragment.class.getName());
        transaction.commit();

        final String[] split = getSupportActionBar().getTitle().toString().split("[|?]");
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isOnlineModeActivated) {
                    getSupportActionBar().setTitle(title + " | " + split[1]);
                }

                else {
                    getSupportActionBar().setTitle(title);
                }
            }
        });

        if(isOnlineModeActivated) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(true);
        }

        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }
    }

    private void setupVideoInterface() {
        PeerConnectionFactory.initializeAndroidGlobals(
                this,  // Context
                true,  // Audio Enabled
                true,  // Video Enabled
                true,  // Hardware Acceleration Enabled
                null); // Render EGL Context

        PeerConnectionFactory pcFactory = new PeerConnectionFactory();
        pubnubManager = Settings.pubnubManager;

        String username = getIntent().getExtras().getString(Settings.JSON_USERNAME, "");

        if (username.isEmpty()) {
            pnRTCClient = new PnRTCClient(pubnubManager.getKeyProvider().getPubKey(), pubnubManager.getKeyProvider().getSubKey());
        }
        pnRTCClient = new PnRTCClient(pubnubManager.getKeyProvider().getPubKey(), pubnubManager.getKeyProvider().getSubKey(), username);

        // Returns the number of cams & front/back face device name
        String frontFacingCam = VideoCapturerAndroid.getNameOfFrontFacingDevice();

        // Creates a VideoCapturerAndroid instance for the device name
        VideoCapturer capturer = VideoCapturerAndroid.create(frontFacingCam);

        // First create a Video Source, then we can make a Video Track
        localVideoSource = pcFactory.createVideoSource(capturer, this.pnRTCClient.videoConstraints());
        VideoTrack localVideoTrack = pcFactory.createVideoTrack(VIDEO_TRACK_ID, localVideoSource);

        // First we create an AudioSource then we can create our AudioTrack
        AudioSource audioSource = pcFactory.createAudioSource(this.pnRTCClient.audioConstraints());
        AudioTrack localAudioTrack = pcFactory.createAudioTrack(AUDIO_TRACK_ID, audioSource);

        // Then we set that view, and pass a Runnable to run once the surface is ready
        VideoRendererGui.setView(glSurfaceVideoView, new EGLContextReady(pnRTCClient, pcFactory, localVideoTrack, localAudioTrack, getIntent().getExtras()));

        remoteRender = VideoRendererGui.create(0, 0, 50, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, false);
        localRender = VideoRendererGui.create(50, 0, 50, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.video_connection_activity_actionbar, menu);

        if(isOnlineModeActivated) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }

        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_hangup:
                if (isOnlineModeActivated) {
                    showDisconnectDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (isOnlineModeActivated) {
                                Settings.synchronisationManager.send(Settings.INSTRUCTION_HANGUP, null);
                                hangup();
                            }
                        }
                    });
                }

                return true;
            case R.id.action_cancel:
                if (isOnlineModeActivated) {
                    if (Settings.synchronisationManager != null) {
                        Settings.synchronisationManager.send(Settings.INSTRUCTION_BACK_TO_LIBRARY, null);
                    }
                }

                showLibraryFragment();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * On back button pressed, ask if the user is in online-mode
     * If he is in online mode, make a dialog and ask if he really wants to quit the connection
     */
    @Override
    public void onBackPressed() {
        if (!isOnlineModeActivated) {
            super.onBackPressed();
            return;
        }

        showDisconnectDialog(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                VideoConnectionActivity.super.onBackPressed();
            }
        });
    }

    public void switchToOfflineMode() {
        final String[] split = getSupportActionBar().getTitle().toString().split("[|?]");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getSupportActionBar().setTitle(split[0]);
                if (menu == null || menu.size() < 2) {
                    return;
                }
                menu.getItem(0).setVisible(false);
            }
        });

        if (pnRTCClient != null) {
            pnRTCClient.closeAllConnections();
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                glSurfaceVideoView.setVisibility(View.GONE);
            }
        });
        isOnlineModeActivated = false;
    }

    private void hangup() {
        pnRTCClient.closeConnection(Settings.currentConnectedUser);
        if (this.localVideoSource != null) {
            this.localVideoSource.stop();
        }
        if (this.pnRTCClient != null) {
            this.pnRTCClient.onDestroy();
        }
        glSurfaceVideoView.setVisibility(View.INVISIBLE);
    }

    private void showDisconnectDialog(DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.dialog_exit_title))
                .setMessage(getResources().getString(R.string.dialog_exit_message))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, listener)
                .create().show();
    }

    /**
     * Switches the book-fragment with the library fragment
     */
    private void showLibraryFragment() {
        Fragment libraryFragment = new LibraryFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, libraryFragment, LibraryFragment.class.getName());
        transaction.commit();

        final String[] split = getSupportActionBar().getTitle().toString().split("[|?]");
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isOnlineModeActivated) {
                    getSupportActionBar().setTitle("Library" + " | " + split[1]);
                }

                else {
                    getSupportActionBar().setTitle("Library");
                }
            }
        });

        if(isOnlineModeActivated) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }

        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
        }
    }

    /**
     * Implementation of the observer
     * The update method for the different instructions
     * If an instruction-type gets called the corresponding instruction is executed
     *
     * @param o   The observable
     * @param arg The argument, which holds the json-object, which holds the instruction-type and the instruction itself
     */
    @Override
    public void update(Observable o, Object arg) {
        JSONObject object = (JSONObject) arg;
        final String type = Settings.synchronisationManager.getConvertedType(object);
        final String instruction = Settings.synchronisationManager.getConvertedInstruction(object);

        switch (type) {
            case Settings.INSTRUCTION_BACK_TO_LIBRARY:
                Fragment libraryFragment = new LibraryFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, libraryFragment);
                transaction.commit();
                break;

            case Settings.INSTRUCTION_SHARE_IDENTITY:
                Log.i("Received other", "Received other");
                Gson gson = new Gson();
                User connectedUser = gson.fromJson(instruction, User.class);
                UserHelper.addFriendToCurrentUser(this, connectedUser);
                break;

            case Settings.INSTRUCTION_HANGUP:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isOnlineModeActivated) {
                            hangup();
                        }
                    }
                });
                break;
        }
    }

    private class CustomRTCListener extends PnRTCListener {
        @Override
        public void onLocalStream(final MediaStream localStream) {
            super.onLocalStream(localStream); // Will log values
            VideoConnectionActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (localStream.videoTracks.size() == 0) return;
                    localStream.videoTracks.get(0).addRenderer(new VideoRenderer(localRender));
                }
            });
        }

        @Override
        public void onAddRemoteStream(final MediaStream remoteStream, final PnPeer peer) {
            super.onAddRemoteStream(remoteStream, peer); // Will log values
            VideoConnectionActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(VideoConnectionActivity.this, "Connected to " + peer.getId(), Toast.LENGTH_SHORT).show();
                    try {
                        if (remoteStream.audioTracks.size() == 0 || remoteStream.videoTracks.size() == 0)
                            return;
                        remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
                        VideoRendererGui.update(remoteRender, 0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, false);
                        VideoRendererGui.update(localRender, 0, 55, 50, 60, VideoRendererGui.ScalingType.SCALE_ASPECT_FIT, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onMessage(PnPeer peer, Object message) {
            if (!(message instanceof JSONObject)) return; //Ignore if not JSONObject

            try {
                Log.d("INSTRUCTION: ", String.valueOf(((JSONObject) message).get(Settings.JSON_INSTRUCTION_TYPE)));
                Log.d("TYPE: ", String.valueOf(((JSONObject) message).get(Settings.JSON_INSTRUCTION)));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Settings.synchronisationManager.notifyObservers(message);
        }

        @Override
        public void onPeerConnectionClosed(PnPeer peer) {
            super.onPeerConnectionClosed(peer);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            switchToOfflineMode();
        }

        @Override
        public void onPeerStatusChanged(PnPeer peer) {
            final String name = peer.getId();

            if (peer.getStatus().equals(PnPeer.STATUS_DISCONNECTED)) {

                VideoConnectionActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(VideoConnectionActivity.this, name + " has disconnected.", Toast.LENGTH_SHORT).show();
                    }
                });

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                switchToOfflineMode();
            }
        }
    }

    private class EGLContextReady implements Runnable {
        PnRTCClient rtcClient;
        PeerConnectionFactory pcFactory;
        VideoTrack localVideoTrack;
        AudioTrack localAudioTrack;
        Bundle extras;

        EGLContextReady(PnRTCClient rtcClient, PeerConnectionFactory pcFactory, VideoTrack localVideoTrack, AudioTrack localAudioTrack, Bundle extras) {
            GLES20.glClearColor(0f, 0f, 0f, 1f);
            this.rtcClient = rtcClient;
            this.pcFactory = pcFactory;
            this.localVideoTrack = localVideoTrack;
            this.localAudioTrack = localAudioTrack;
            this.extras = extras;
        }

        @Override
        public void run() {
            // We start out with an empty MediaStream object, created with help from our PeerConnectionFactory
            // Note that LOCAL_MEDIA_STREAM_ID can be any string
            MediaStream mediaStream = pcFactory.createLocalMediaStream(LOCAL_MEDIA_STREAM_ID);

            // Now we can add our tracks.
            mediaStream.addTrack(localVideoTrack);
            mediaStream.addTrack(localAudioTrack);

            // First attach the RTC Listener so that callback events will be triggered
            rtcClient.attachRTCListener(new CustomRTCListener());

            // Then attach your local media stream to the PnRTCClient.
            //  This will trigger the onLocalStream callback.
            rtcClient.attachLocalMediaStream(mediaStream);

            // Listen on a channel. This is your "phone number," also set the max chat users.
            rtcClient.listenOn(Settings.currentActiveUser.getDisplayName());
            rtcClient.setMaxConnections(1);

            // If the intent contains a number to dial, call it now that you are connected.
            //  Else, remain listening for a call.
            if (extras.containsKey(Settings.JSON_CALL_USER) && extras.containsKey(Settings.JSON_HAS_DIALED)) {
                String callUser = extras.getString(Settings.JSON_CALL_USER);
                boolean hasDialed = extras.getBoolean(Settings.JSON_HAS_DIALED);
                rtcClient.connect(callUser, hasDialed);
            }
        }
    }
}