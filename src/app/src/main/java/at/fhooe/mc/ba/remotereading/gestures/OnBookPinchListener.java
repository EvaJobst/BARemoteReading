package at.fhooe.mc.ba.remotereading.gestures;

import android.view.ScaleGestureDetector;

import java.util.ArrayList;

/**
 * Created by Eva on 19.02.2018.
 */

public class OnBookPinchListener extends ScaleGestureDetector.SimpleOnScaleGestureListener implements BookObservable {
    ArrayList<BookObserver> bookObserverList;
    float scaleFactor;

    public OnBookPinchListener() {
        bookObserverList = new ArrayList<>();
        scaleFactor = 1.0f;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        scaleFactor *= detector.getScaleFactor();
        // Don't let the object get too small or too large.
        scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
        notifyBookObserver(scaleFactor);
        return true;
    }

    @Override
    public void addBookObserver(BookObserver bookObserver) {
        bookObserverList.add(bookObserver);
    }

    @Override
    public void removeBookObserver(BookObserver bookObserver) {
        bookObserverList.remove(bookObserver);
    }

    @Override
    public void notifyBookObserver(Object object) {
        if(object instanceof Float) {
            for (BookObserver bookObserver : bookObserverList) {
                bookObserver.notifyBookObserver((float)object);
            }
        }
    }
}
