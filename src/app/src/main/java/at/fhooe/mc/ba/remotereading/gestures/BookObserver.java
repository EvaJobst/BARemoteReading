package at.fhooe.mc.ba.remotereading.gestures;

/**
 * Created by Eva on 19.02.2018.
 */

public interface BookObserver {
    void notifyBookObserver(float x, float y);
    void notifyBookObserver(float scale);
    void notifyBookObserver(boolean isLeftSwipe);
}
