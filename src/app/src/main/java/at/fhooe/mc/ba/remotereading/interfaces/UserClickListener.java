package at.fhooe.mc.ba.remotereading.interfaces;

public interface UserClickListener {
    void onUserClick(int position);
}
